pragma solidity ^0.4.17;

contract CBSToken {

    address admin;
    string public name = "CBSToken";
    string  public symbol = "CBST";
    string  public standard = "CBS Token v1.1";
    uint8   public decimals = 2;
    uint256 public totalSupply;

    mapping(address => uint256) public balanceOf;
    mapping(address => mapping(address => uint256)) public allowance;


    // Event
    event Transfer(
        address indexed _from,
        address indexed _to,
        uint256 _value
    );

    event Approval(
        address indexed _owner,
        address indexed _spender,
        uint256 _value
    );

    event Deposit(address _admin, uint256 _amount);
    event ChangeAdmin(address _oldAdmin, address _newAdmin);


    // Constructor
    function CBSToken() public {
        admin = msg.sender;

        uint _initialSupply = 100000000000;
        balanceOf[msg.sender] = _initialSupply;
        totalSupply = _initialSupply; 
    }

    function transfer(address _to, uint256 _value) public returns (bool success) {
        require(balanceOf[msg.sender] >= _value);

        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;

        Transfer(msg.sender, _to, _value);

        return true;
    }

    function approve(address _spender, uint256 _value) public returns (bool success) {
        allowance[msg.sender][_spender] = _value;

        Approval(msg.sender, _spender, _value);

        return true;
    }

    function transferFrom(address _from, address _to, uint256 _value) public returns (bool success) {
        require(_value <= balanceOf[_from]);
        require(_value <= allowance[_from][msg.sender]);

        balanceOf[_from] -= _value;
        balanceOf[_to] += _value;
        allowance[_from][msg.sender] -= _value;

        Transfer(_from, _to, _value);

        return true;
    }

    function deposit(uint256 _value) public returns (bool success) {
        require(msg.sender == admin);

        balanceOf[msg.sender] += _value;
        totalSupply += _value; 

        Deposit(msg.sender, _value);
        return true;
    }

    function changeAdmin(address _to) public returns (bool success) {
        require(msg.sender == admin);

        address _oldAdmin = admin;
        admin = _to;

        ChangeAdmin(_oldAdmin, admin);
        return true;
    }
}