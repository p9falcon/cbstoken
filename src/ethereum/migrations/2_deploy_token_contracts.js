var CBSToken = artifacts.require("./CBSToken.sol");
var CBSTokenSale = artifacts.require("./CBSTokenSale.sol");

module.exports = function(deployer) {

  // Deploy contract 
  deployer.deploy(CBSToken).then(function() {
    // Token price is .001 Ether, or $1 USD @ $1k USD/ether
    var tokenPrice = 1000000000000000; // in wei
    return deployer.deploy(CBSTokenSale, CBSToken.address, tokenPrice);
  });

};
