App = {
  web3Provider: null,
  contracts: {},
  myAddress: '0x2C8773EFA576fDee3c59a5BefbFb958B1d1A68c6', // SET USER'S ADDRESS
  contractAddress: '0x7A2894F844dA156B6F43390d98010719366FA580',
  contractABI: [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"standard","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"changeAdmin","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"deposit","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_admin","type":"address"},{"indexed":false,"name":"_amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_oldAdmin","type":"address"},{"indexed":false,"name":"_newAdmin","type":"address"}],"name":"ChangeAdmin","type":"event"}],
  account: '0x0',
  loading: false,
  tokensSold: 0,


  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    console.log("- initWeb3()")
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
      return App.startApp(web3);
    } 

    console.log("- ERROR! Metamask is not conntected.")
    return null;
  },

  startApp: function(web3) {
    console.log("- startApp()")
    const eth = new Eth(web3.currentProvider)
    const contract = new EthContract(eth)
    App.initContract(contract)

    return null;
  },

  initContract: function(contract) {
    console.log("- initContract()");
    const MiniToken = contract(App.contractABI)
    const miniToken = MiniToken.at(App.contractAddress)
    App.listenForClicks(miniToken)
  },

  listenForClicks: function(miniToken) {
    console.log("- listenForClicks()");
    var button = document.querySelector('button.transferFunds')
    button.addEventListener('click', function() {

      // Admin account. DO NOT CHANGE THIS ACCOUNT!!!!
      const toAddress = "0x2C8773EFA576fDee3c59a5BefbFb958B1d1A68c6"
      const value = 10
      miniToken.transfer(toAddress, value, { from: App.myAddress })
      .then(function (txHash) {
        // NOW, USER ABLE TO WATCH VIDEO!!!!!
        console.log('Transaction sent')
        console.dir(txHash)
        App.waitForTxToBeMined(txHash)
      }) .catch(console.error) })
     },

  waitForTxToBeMined: async function(txHash) {
    console.log("- waitForTxToBeMined() START.")
    let txReceipt
    while (!txReceipt) {
      try {
        txReceipt = await eth.getTransactionReceipt(txHash)
      } catch (err) {
        return indicateFailure(err)
      }
    }

    console.log("- waitForTxToBeMined() END.")
    //indicateSuccess()
  },

  render: function() {
    if (App.loading) {
      return;
    }
    App.loading = true;

    var dappTokenSaleInstance;
    var dappTokenInstance;

    var loader = $("#loader");
    var content = $("#content");

    loader.show();
    content.hide();

    // Load account data
    web3.eth.getCoinbase(function(err, account) {
      if (err === null) {
        App.account = account;
        $("#accountAddress").html("Your Account: " + account);
      }
    });
  }
};

$(function() {
  $(window).load(function() {
    App.init();
  });
});
