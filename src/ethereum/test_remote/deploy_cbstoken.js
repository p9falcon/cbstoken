var Tx = require('ethereumjs-tx')
var config = require('./config.js')
const Web3 = require('web3')
const web3 = new Web3(config.ropstenAddress())

console.log('START.')

web3.eth.getTransactionCount(account, (err, txCount) => {

  console.log('err:', err, 'txCount:', txCount)

  // Create transaction object
  const txObject = {
    nonce: web3.utils.toHex(txCount),
    gasLimit: web3.utils.toHex(4700000),
    gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
    data: config.contractData()
  }

  // Sign the transaction
  const tx = new Tx(txObject)
  tx.sign(config.adminPrivateKey())
  const serializedTx = tx.serialize()
  const raw = '0x' + serializedTx.toString('hex')

  // Broadcast the transaction
  web3.eth.sendSignedTransaction(raw, (err, txHash) => {
    console.log('err:', err, 'txHash:', txHash)
  })
})