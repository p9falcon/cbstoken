var Tx = require('ethereumjs-tx')
var config = require('./config.js')
const Web3 = require('web3')
const web3 = new Web3(config.ropstenAddress())

// Contract
var contract = new web3.eth.Contract(config.contractABI(), config.contractAddress())


// Transfer some tokens
web3.eth.getTransactionCount(config.adminAccount(), (err, txCount) => {

  const txObject = {
    nonce:    web3.utils.toHex(txCount),
    gasLimit: web3.utils.toHex(4700000), // Raise the gas limit to a much higher amount
    gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei')),
    to: config.contractAddress(),
    data: contract.methods.transfer(config.nickAccount(), 100000000).encodeABI()
  }

  // Sign the transaction
  const tx = new Tx(txObject)
  tx.sign(config.adminPrivateKey())
  const serializedTx = tx.serialize()
  const raw = '0x' + serializedTx.toString('hex')

  web3.eth.sendSignedTransaction(raw, (err, txHash) => {
    console.log('err:', err, 'txHash:', txHash)
    // Use this txHash to find the contract on Etherscan!
  })
})

// Check Token balance for account
contract.methods.balanceOf(config.adminAccount()).call((err, balance) => {
  console.log('balance of admin:', balance)
})

// Check Token balance for targetUser
contract.methods.balanceOf(config.testUserAccount()).call((err, balance) => {
  console.log('balance of target user:', balance)
})


