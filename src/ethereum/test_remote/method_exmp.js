var config = require('./config.js')
const Web3 = require('web3')
const web3 = new Web3(config.ropstenAddress())

dappTokenContract = new web3.eth.Contract(config.contractABI(), config.contractAddress())

dappTokenContract.methods.balanceOf(config.adminAccount()).call((err, name) => {
  console.log('balance of admin:', name)
})

dappTokenContract.methods.balanceOf(config.testUserAccount()).call((err, name) => {
  console.log('balance of target user:', name)
})
