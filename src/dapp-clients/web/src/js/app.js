App = {
  web3Provider: null,
  contracts: {},
  userAddress: '0x0', // SET USER'S ADDRESS
  contractAddress: '0x7A2894F844dA156B6F43390d98010719366FA580',
  adminAddress: '0x2C8773EFA576fDee3c59a5BefbFb958B1d1A68c6',
  contractABI: [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"standard","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"changeAdmin","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"deposit","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"address"}],"name":"allowance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_admin","type":"address"},{"indexed":false,"name":"_amount","type":"uint256"}],"name":"Deposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_oldAdmin","type":"address"},{"indexed":false,"name":"_newAdmin","type":"address"}],"name":"ChangeAdmin","type":"event"}],
  account: '0x0',
  miniToken: null,
  transValue: 10,
  tokenRequestUrl: 'https://l2vtfxq6y2.execute-api.us-east-1.amazonaws.com/dev/sharecontent/',

  init: function() {
    // Get url parameter on page load
    var curUrl = $(location);
    var parameters = curUrl.attr('search');
    var getTokenUrl = App.tokenRequestUrl + parameters;
    console.log(parameters);

    if (parameters) {
      // For Demo - using GET request
      App.httpGetAsync(getTokenUrl);
    }


    /*

    // Post request
    var url = "https://hl6z7kqu1k.execute-api.us-east-1.amazonaws.com/dev/sharecontent";
    var method = "POST";
    var postData = "Some data";

    var shouldBeAsync = true;

    var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
    var request = new XMLHttpRequest();

    request.onload = function () {
       var status = request.status; // HTTP response status, e.g., 200 for "200 OK"
       var data = request.responseText; // Returned data, e.g., an HTML document.
    }

    request.open(method, url, shouldBeAsync);

    request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    //request.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
  



    // Sends the request to the server.
    request.send(uuid);
    */
    
    return App.initWeb3();
  },

  httpGetAsync: function(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
  },


  initWeb3: function() {
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(App.web3Provider);

      // get user address
      web3.eth.getCoinbase(function(err, account) {
        if (err === null) {
          App.userAddress = account;

          console.log("User address: " + App.userAddress);
        }
      });

      return App.initContract(web3);

    } else {

      // Tell user to have Matamask installed
      // or use a web3 browser
    }

    return null
  },

  initContract: function(web3) {
    const eth = new Eth(web3.currentProvider);
    const contract = new EthContract(eth);

    const MiniToken = contract(App.contractABI);
    App.miniToken = MiniToken.at(App.contractAddress);

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '#pay-tokens', App.handleTransfer);
    $(document).on('click', '#fb-share-btn', App.handleShare);
  },

  grantAccess: function() {
    // hide the paywall
    $('#paywall').css({ display: "none" });
  },

  handleTransfer: function(event) {
    event.preventDefault();

    App.miniToken.transfer(App.adminAddress, App.transValue, {from: App.userAddress})
    .then(function(txHash) {

      // Update UI
      App.grantAccess();

      console.log('Tranaction sent');
      console.dir(txHash);
      return App.waitForTxToBeMined(txHash);
    }).catch(function(err) {
      console.log(err.message);
    });
  },

  handleShare: function(event) {
    event.preventDefault();

    return App.fbShare(App.userAddress);

    /*
    console.log("share event handling");

    var shareId = parseInt($(event.target).data('id'));

    var adoptionInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.Adoption.deployed().then(function(instance) {
        adoptionInstance = instance;
        //return adoptionInstance.adopt(shareId, {from: account});
        return App.fbShare(account);
      }).then(function(result) {
        // TODO: make a markShared function
        return App.markeAdopted();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
    */
  },


  fbShare: function(account) {

    var shareUrl = $('.social-share').data('social-links-options')['url'];
    var uuid= App.uuid();

    if (account) {
      FB.ui({
        method: 'share',
        href: shareUrl + '?add=' + account + '&uuid=' + uuid
      }, function(response){});
    }
  },

  uuid: function() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    )
  },

  waitForTxToBeMined: async function(txHash) {
    console.log("- waitForTxToBeMined() START.")
    let txReceipt
    while (!txReceipt) {
      try {
        txReceipt = await eth.getTransactionReceipt(txHash)
      } catch (err) {
        return indicateFailure(err)
      }
    }

    console.log("- waitForTxToBeMined() END.")
    //indicateSuccess()
  },

  httpGetAsync: function(theUrl, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
  }
};



$(function() {
  $(window).load(function() {
    App.init();
  });
});
