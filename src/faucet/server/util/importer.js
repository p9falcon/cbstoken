'use strict';
var rfr = require('rfr');
var logger = rfr('util/logger');
var cognito = rfr('util/cognito');
var socialshares = rfr('lambda/socialshares');

/*
 More information about the UserGroups and the precedence
 http://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-user-groups.html#assigning-precedence-values-to-groups
 */

var SampleGroups = [
  {
    name: 'adminGroup',
    description: 'Cognito user group for administrators',
    precedence: 0,
    roleArn: 'cognitoAuthAdminRoleArn'
  },
  {
    name: 'clientGroup',
    description: 'Cognito user group for faucetserver users',
    precedence: 1,
    roleArn: 'cognitoAuthStandardRoleArn'
  },
];

let adminUserName = 'test_admin';

var SampleUsers = [
  {
    username: adminUserName,
    email: 'admin@example.com',
    givenName: 'Admin',
    familyName: 'User',
    password: '1234ABcd!'

  },
  {
    username: 'test_user',
    email: 'user1@example.com',
    givenName: 'Sample',
    familyName: 'User',
    password: '5678ABcd!'
  }
];

class SampleData {

  constructor() {
  }

  generateSampleData() {
    return Promise.all([
      this.generateSampleSocialShare('0x2C8773EFA576fDee3c59a5BefbFb958B1d1A68c6', 'fDee3c59a5B', 324568)
        .then(shareId => {
        }),
      this.generateSampleSocialShare('0x304C8133bb900c201DC52D1e9b9607Ad01eF9C58', 'Q900c201DC52', 170223)
        .then(shareId => {
        }),
        this.generateSampleSocialShare('0xf17f52151EbEF6C7334FAD080c5704D77216b732', 'EbEF6C7334FA', 3125)
        .then(shareId => {
        }),
        this.generateSampleSocialShare('0x304C8133bb900c201DC52D1e9b9607Ad01eF9C58', 'bb900c201Do3', 12963)
        .then(shareId => {
        })
    ])
  }

  generateSampleSocialShare(userAccount, randomUUID, visitCount) {
    return new Promise((resolve, reject) => {

      socialshares.Create({
          body: JSON.stringify({
            userAccount: userAccount,
            randomUUID: randomUUID,
            visitCount: visitCount,
          })
        },
        {/* Empty context object */},
        (err, out) => {
          if (err !== null) {
            reject(err);
          } else {
            // return the shareId
            let data = JSON.parse(out.body);
            resolve(data.shareId);
          }
        }
      );
    });
  }

  static createPromiseToCreateUser(user) {
    let promise = new Promise((resolve, reject) => {
      cognito.adminCreateUser(user)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
    return promise;
  }

  static generateSampleUsers() {
    let promises = [];
    for (let user of SampleUsers) {
      // create a Promise for each user creation task
      let promise = SampleData.createPromiseToCreateUser(user);
      promises.push(promise);
    }
    // now, run all those Promises in parallel
    return Promise.all(promises);
  }

  //TODO: Update following method to accept a particular username or user details object and lookup their corresponding user identity pools Id
  static getSampleUser() {
    return new Promise((resolve) => {
      let user = SampleUsers[1];
      cognito.getIdentityPoolUserId(user).then((data) => {
        // logger.info(data);
        resolve(data);
      });
    });
  }

  static createPromiseToCreateGroup(group) {
    let promise = new Promise((resolve, reject) => {
      cognito.adminCreateGroup(group)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
    return promise;
  }

  static generateSampleUserGroups() {
    let promises = [];
    for (let group of SampleGroups) {
      let promise = SampleData.createPromiseToCreateGroup(group);
      promises.push(promise);
    }
    return Promise.all(promises);
  }


  static createUserAssignmentToGroupPromise(user, group) {
    let promise = new Promise((resolve, reject) => {
      cognito.adminAssignUserToGroup(user, group)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => {
          reject(err);
        });
    });
    return promise;
  }


  static assignUsersToGroups() {
    let promises = [];
    for (let user of SampleUsers) {
      let group = null;

      if (user.username === adminUserName) {
        group = SampleGroups[0];
      } else {
        group = SampleGroups[1];
      }
      let promise = SampleData.createUserAssignmentToGroupPromise(user, group);
      promises.push(promise);
    }
    logger.info(promises.length);
    return Promise.all(promises);


  }


} // end class

module
  .exports = {
  SampleData
};
