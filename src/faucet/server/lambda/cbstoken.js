'use strict';
var Tx = require('ethereumjs-tx');
let rfr = require('rfr');
let wrapper = rfr('wrapper');
var config = rfr('/cbstoken_config.js');
var conf = config.config;
var Web3 = require('web3');
var web3 = new Web3(
  new Web3.providers.HttpProvider(conf.ROPSTEN_ADDR)
);


function _TransferToken(targetAddress, tokenAmount) {

  console.log('Simon Log:  _TransferToken:', targetAddress)

  return new Promise(function(resolve, reject) {

    web3.eth.getTransactionCount(conf.ADMIN_ACCOUNT, function(err, txCount) {
      if (err) {
        console.log("- ERROR!   web3.eth.getTransactionCount()")
        reject(err)
      }
      else {
        // Contract
        let contract = web3.eth.contract(conf.CONTRACT_ABI).at(conf.CONTRACT_ADDR);

        let randomNo = Math.floor(Math.random() * 5);
        let nonceNo = txCount + randomNo;
        console.log("randomNo: " + randomNo)

        let txObject = {
          nonce:    web3.toHex(nonceNo),
          gasLimit: web3.toHex(4700000), // Raise the gas limit to a much higher amount
          gasPrice: web3.toHex(20000000000),
          to: conf.CONTRACT_ADDR,
          data: contract.transfer.getData(targetAddress, tokenAmount)
        }

        // Sign the transaction
        let tx = new Tx(txObject)
        const privateKey = new Buffer(conf.ADMIN_PRIVATEKEY, 'hex')
        tx.sign(privateKey)
        let serializedTx = tx.serialize().toString('hex')
        let raw = '0x' + serializedTx

        web3.eth.sendRawTransaction(raw, function(err, txHash) {
        console.log('err:', err, 'txHash:', txHash)
          if (err) {
            reject(err)
          }
          else {
            let result = {
              "userAccount" : targetAddress, 
              "txHash" : txHash 
            };
            resolve(result);
          }
        })
      }
    })
  })
}


function GiveMeFreeToken(event) {
  console.log(" params:" + event.pathParameters)

  if(!event.pathParameters || !event.pathParameters.userAccount) {
    return Promise.reject({"error": "userAddress is invalid"})
  } 
  else {
    const toAddress =  event.pathParameters.userAccount;
    if(toAddress.length != 42) {
      return Promise.reject({"error": "account format is invalid."}) 
    }
    else if(!toAddress || !toAddress.trim()) {
      return Promise.reject({"error": "userAddress is invalid."})
    }
    else {
      console.log('Simon Log:   GiveMeFreeToken:', toAddress)
     return _TransferToken(toAddress, 500)
    }
  }
}

function ContentIsLoaded(event) {
  console.log('SIMON LOG: ContentIsLoaded().. ');
  console.log(event);

  if (!event.queryStringParameters) {
    console.log('SIMON LOG: ContentIsLoaded() query string is null.. ');
    return new Promise(function(resolve, reject){
      resolve( "failed" );
    })
  }
  
  else {
    const toAddress =  event.queryStringParameters.add;
    const uuid = event.queryStringParameters.uuid;

    if(toAddress.length != 42) {
      return new Promise(function(resolve, reject){
        resolve( "invalid account format" );
      })
    }
    else if(!toAddress || !toAddress.trim()) {
      return new Promise(function(resolve, reject){
        resolve( "invalid account" );
      })
    }
    else if(!uuid || !uuid.trim()) {
      return new Promise(function(resolve, reject){
        resolve( "invalid uuid" );
      })
    }
    else {
      console.log("useraccount: " + toAddress);
      console.log("uuid: " + uuid);

      return _TransferToken(toAddress, 10)
    }
  }
} 
 

module.exports = wrapper.wrapModule({
  _TransferToken,
  GiveMeFreeToken,
  ContentIsLoaded
});


