'use strict';
let rfr = require('rfr');
let data = rfr('data');
let cbstoken = rfr('cbstoken');
let wrapper = rfr('wrapper');
let SocialSharesTable = new data.SocialSharesTable();


function Create(event) {
  console.log('SIMON LOG: socialshares-Create()');
  console.log(event);
  return SocialSharesTable.put(JSON.parse(event.body));
}

function Get(event) {
  return SocialSharesTable.get(event.pathParameters.shareId);
}

function List(event) {
  console.log(event);
  return SocialSharesTable.scan();
}

function Update(event) {
  let input = JSON.parse(event.body);
  return SocialSharesTable.get(event.pathParameters.shareId).then((data) => {
    input.createTime = data.createTime;
    input.shareId = event.pathParameters.shareId;
    return SocialSharesTable.put(input);
  })
}

function GetShareContent(event) {
  console.log('SIMON LOG: GetShareContent(). event:' + event);
}

function ListByUserAccount(event) {
  console.log('SIMON LOG: ListByUserAccount(). event:' + event);

  return SocialSharesTable.listByUserAccount(event.pathParameters.userAccount).then(function(items) {
    return { items };
  }).catch(function(err) {
    console.log(err);
    return Promise.reject(err);
  });
}

module.exports = wrapper.wrapModule({
  Create,
  Get,
  List,
  Update,
  GetShareContent,
  ListByUserAccount
});


