import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { FaucetPage } from '../pages/faucet/faucet';
import { TokenSalePage } from '../pages/tokensale/tokensale';
import { AboutPage } from '../pages/about/about';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// services
import { HttpService } from '../services/http-service';
import { AlertService } from '../services/alert.service';
import { Web3Service } from '../services/web3.service'; 

import {
 NoAuthorizationClient
} from "../services/authorizer-aws.service";


@NgModule({
  declarations: [
    MyApp,
    FaucetPage,
    TokenSalePage,
    AboutPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FaucetPage,
    TokenSalePage,
    AboutPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AlertService,
    Web3Service,
    { provide: HttpService, useClass: HttpService },
    { provide: NoAuthorizationClient, useClass: NoAuthorizationClient },
    { provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
