import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';
import  Web3 from 'web3';

//platformBrowserDynamic().bootstrapModule(AppModule);



window.addEventListener('load', function() {

    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof window.web3 !== 'undefined') {
        console.log("- Metamask is available!")
        window.web3 = new Web3(window.web3.currentProvider);

            // Load account data
    //     window.web3.eth.getCoinbase(function(err, account) {
    //         console.log("my account: " + account);
    //     if (err === null) {
    //         console.log("No error!!");
    //     }
    //   });
    }  
    platformBrowserDynamic().bootstrapModule(AppModule);
  
  });