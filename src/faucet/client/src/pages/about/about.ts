import { Component } from '@angular/core';
import { DefaultService } from "../../services/faucetserver-sdk/api/default.service";
import { Web3Service } from'../../services/web3.service'; 
import { AlertService } from '../../services/alert.service';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  myAccount: string;
  userAccountRopstenScanUrl: string;

  constructor(private alertService: AlertService,
    private web3Service: Web3Service,
    private loadingCtrl: LoadingController) {
      console.log("- AboutPage:constructor()");
      // Load account data
      var web3 = web3Service.getWeb3();
      web3.eth.getCoinbase(function(err, account) {
        if (err === null) {
          console.log("my Ethereum account: " + account);
          web3Service.myAccount = account;
        }
      })

      let timer = setTimeout(x => 
        {
          this.myAccount = this.web3Service.myAccount;
          if(this.myAccount) {
            this.userAccountRopstenScanUrl = "https://ropsten.etherscan.io/address/" + this.myAccount;
          }
        }, 1000); 
  }
}
