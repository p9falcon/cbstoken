import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertService } from '../../services/alert.service';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { NoAuthorizationClient } from "../../services/authorizer-aws.service";
import { DefaultService } from "../../services/faucetserver-sdk/api/default.service";
import { SocialShare } from '../../services/faucetserver-sdk/model/socialShare';


@Component({
  selector: 'page-faucet',
  templateUrl: 'faucet.html'
})
export class FaucetPage {

  socialUrl: string;
  tokenAPI: DefaultService;
  isRequested: Boolean = false;

  constructor(public navCtrl: NavController,
    private noAuthClient: NoAuthorizationClient,
    private loadingCtrl: LoadingController,
    private alertService: AlertService) {
      this.tokenAPI = new DefaultService(<any>noAuthClient.httpService, null, null);
 
  }


  onGiveMeToken() {
    console.log("onGiveMeToken() socialUrl:" + this.socialUrl);
    const facekbookPrefix = "https://www.facebook.com/permalink.php";
    if (!this.socialUrl || this.socialUrl.length < facekbookPrefix.length + 10) {
      this.alertService.presentAlert('Invalid Facebook URL', 'Please enter Facebook URL containing your Ethereum address.');
      return
    }

    if (!this.socialUrl.includes(facekbookPrefix)) {
      this.alertService.presentAlert('Invalid Facebook URL', 'Please enter Facebook URL containing your Ethereum address..');
      return
    }

    let tokenLoading = this.loadingCtrl.create({
      content: 'Requesting...'
    });

    tokenLoading.present().then(()=>{

      var socialShare: SocialShare = {
        shareId: "",
        randomUUID: "randomUUID0",
        userAccount: "userAccount0",
        visitCount: 0
      }

      this.tokenAPI.socialsharesCreate(socialShare).subscribe(
        (data) => {
          console.log("- socialsharesCreate() success...")
          console.log(data);
          this.isRequested = true;
          tokenLoading.dismiss().catch(() => console.log('FB sharing verification request success'));

          this.alertService.presentAlert('Success!', 'You will now receive CBS tokens when people click your URL.');
        },
        (err) => {
          console.log("- socialsharesCreate() failed...")
          console.error(err);
          this.isRequested = true;
          tokenLoading.dismiss().catch(() => console.log('FB sharing verification request failed'));
          this.alertService.presentAlert('Success!', 'You will now receive CBS tokens when people click your URL.');
        }

      )
    });
  }

}
