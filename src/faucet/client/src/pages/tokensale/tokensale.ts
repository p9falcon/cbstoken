import { Component } from '@angular/core';
import { NoAuthorizationClient } from "../../services/authorizer-aws.service";
import { DefaultService } from "../../services/faucetserver-sdk/api/default.service";
import { Web3Service } from'../../services/web3.service'; 
import { AlertService } from '../../services/alert.service';
import { AlertController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';

@Component({
  selector: 'page-tokensale',
  templateUrl: 'tokensale.html'
})
export class TokenSalePage {

  userAccount: string;
  tokenAPI: DefaultService;

  constructor(private alertService: AlertService,
    private web3Service: Web3Service,
    private noAuthClient: NoAuthorizationClient,
    private loadingCtrl: LoadingController) {
      console.log("- TokenSalePage:constructor()");
      this.tokenAPI = new DefaultService(<any>noAuthClient.httpService, null, null);


      // Load account data
      var web3 = web3Service.getWeb3();
      web3.eth.getCoinbase(function(err, account) {
        if (err === null) {
          console.log("my Ethereum account: " + account);
          web3Service.myAccount = account;
        }
      })

      let timer = setTimeout(x => 
        {
          console.log("timer is fired.");
          this.userAccount = this.web3Service.myAccount;
        }, 1000);
  }

  onGiveMeFreeToken() {

    console.log("- onGiveMeFreeToken() userAccount:" + this.userAccount);
    if (!this.userAccount || this.userAccount.length != 42) {
      console.log("- onGiveMeFreeToken() user account is invalid.");
      this.alertService.presentAlert('Invalid user address', 'Please enter your account of Ropsten test network.');
      return
    }

    let tokenLoading = this.loadingCtrl.create({
      content: 'Requesting free CBS Token...'
    });

    tokenLoading.present().then(()=>{

      this.tokenAPI.cbstokenGiveMeFreeToken(this.userAccount).subscribe(
        (data) => {
          console.log("- free token api result()...")
          console.log(data);

          const body = data._body;
          if(body) {
            let txNo = body.txHash;
            console.log("- txNo: " + txNo);
            if (txNo && txNo != "") {
              this.makeTransactionLink(txNo);
            }

            tokenLoading.dismiss().catch(() => console.log('tokenLoading success'));
          }
          else {
           tokenLoading.dismiss().catch(() => console.log('body is invalid'));
          }
        },
         (err) => {
           console.log("- free token api result() is failed...")
           console.error(err);
           tokenLoading.dismiss().catch(() => console.log('tokenLoading err'));
         }
       )
    });
  }

  
  makeTransactionLink(txNo: string) {
    console.log("- makeTransactionLink() txUrl: " + txNo);
    var str = "Your request was accepted. (Click to see your transaction. It may take some time for your transaction to complete.)";
    var txUrl = "https://ropsten.etherscan.io/tx/" + txNo;
    var result = str.link(txUrl);
    document.getElementById("txLink").innerHTML = result;
  }

  private handleError(error: any) {
    console.error('noti.component.ts  An error occurred', error);
    this.alertService.presentAlert('An error occurred', error);
  }

}
