import { Http, RequestOptions } from '@angular/http';
import { Injectable } from "@angular/core";
import { Logger } from './logger.service';
import { HttpService } from "./http-service";
import { HttpClientModule } from '@angular/common/http'; 
import { HttpModule } from '@angular/http';

@Injectable()
export class AuthorizerBase {
    public httpService: HttpService;

    constructor(httpService: HttpService) {
      console.log("- AuthorizerBase:constructor()");
      this.httpService = httpService;
    }
}


@Injectable()
export class NoAuthorizationClient extends AuthorizerBase {

  constructor(http: Http) {
    console.log("- NoAuthorizationClient:constructor()");
     let httpService: HttpService = new HttpService(http);
    // httpService.addInterceptor((options: RequestOptions) => {
    //   console.log('%cRequest without authorization:\n', Logger.LeadInStyle, options.method, options.url, '\nHeaders:', options.headers['_headersMap'], '\nBody:', options.body);
    // });

     super(httpService);
  }
}
